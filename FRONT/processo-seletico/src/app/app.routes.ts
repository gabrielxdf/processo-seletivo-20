import {Routes} from "@angular/router";
import {CadastroComponent} from "./cadastro/cadastro.component";
import {ListarComponent} from "./listar/listar.component";

export const ROUTES: Routes = [
  {path: '', component: ListarComponent},
  {path: 'cadastro', component: CadastroComponent}
]
