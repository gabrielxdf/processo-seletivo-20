import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  registerForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      setor: ['', [Validators.required]],
      email: ['', [Validators.required], Validators.email],
      salario: ['', [Validators.required]],
      idade: ['', [Validators.required]],
  })
}


  cadastrar() {
    console.log(this.registerForm.value);
  }
}

