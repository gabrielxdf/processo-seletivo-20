import { Component, OnInit } from '@angular/core';
import axios from "axios";

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    axios.get("http://localhost:8080/funcionarios_war_exploded/rs/funcionarios/consultar").then(response => {this.listaProdutos = response.data;
    }).catch(function (error) {
      mostraAlertaErro();
    })
  }

  listaProdutos: [
    {sortable: false, key: "nome", label:"Nome"},
    {sortable: false, key: "fabricante.nome", label:"Fabricante"},
    {sortable: false, key: "volume", label:"Volume"},
    {sortable: false, key: "unidade", label:"Unidade"},
    {sortable: false, key: "estoque", label:"Estoque"}
  ];
  }
function mostraAlertaErro(){
  alert("Erro interno, Não foi possível listar natureza de serviços");
}

